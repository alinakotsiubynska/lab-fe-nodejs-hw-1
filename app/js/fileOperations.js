const fs = require('fs')
const path = require('path')
const CustomError = require('./CustomError')

const addFile = async ({filename, content, filesPath}) => {
  try {
    if (!fs.existsSync(filesPath)) {
      await fs.promises.mkdir(filesPath)
    }
    const filePath = path.join(filesPath, filename);
    const allFiles = await getFiles(filesPath)

    if (allFiles.includes(filename)) {
      throw new CustomError(400, `File with name '${filename}' already exists`)
    }
    await fs.promises.writeFile(filePath, content)
  } catch (error) {
    throw error
  }
}

const getFiles = async (filesPath) => {
  try {
    const allFiles = await fs.promises.readdir(filesPath)
    return allFiles
  } catch (error) {
    throw error
  }
}

const getFile = async ({filename, filesPath}) => {
  try {
    const filePath = path.join(filesPath, filename);
    const fileContent = await fs.promises.readFile(filePath, 'utf8')
    const updatedTime = (await fs.promises.stat(filePath)).mtime
    const file = {
      content: fileContent,
      extension: filename.split('.').reverse()[0],
      uploadedDate: updatedTime
    }
    return file
  } catch (error) {
    if (error.message.includes('no such file or directory')) {
      throw new CustomError(400, `No file with '${filename}' name found`)
    } else {
      throw error
    }
  }
}

const deleteFile = async ({filename, filesPath}) => {
  try {
    const filePath = path.join(filesPath, filename);
    await fs.promises.unlink(filePath)
  } catch (error) {
    if (error.message.includes('no such file or directory')) {
      throw new CustomError(400, `No file with '${filename}' name found`)
    } else {
      throw error
    }
  }
}

const editFile = async ({filename, filesPath, content}) => {
  try {
    const filePath = path.join(filesPath, filename);
    await fs.promises.readFile(filePath)
    await fs.promises.writeFile(filePath, content)
  } catch (error) {
    if (error.message.includes('no such file or directory')) {
      throw new CustomError(400, `No file with '${filename}' name found`)
    } else {
      throw error
    }
  }
}


  
  
module.exports = {
  addFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}