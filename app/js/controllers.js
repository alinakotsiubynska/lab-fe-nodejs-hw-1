const path = require('path') 
const operations = require('./fileOperations')
const validateFile = require('./validateFile')
const ps = require('./passwordService')
const CustomError = require('./CustomError')
const filesPath = path.join(__dirname, '../files')

const createFile = async (req, res, next) => {
  const { filename, content, password } = req.body;
  try {
    if (!content) {
      throw new CustomError(400,"Please specify 'content' parameter")
    }
    if (!filename) {
      throw new CustomError(400,"Please specify 'filename' parameter")
    }
    validateFile(filename)
    await operations.addFile({ filename, content, filesPath })
    if (password) {
      ps.createPassword({filename, password})
    }
    res.status(200).json({message: "File created successfully"})  
  } catch (error) {
    if (error.status === undefined || error.status === 500) {
      res.status(500).json({message: "Server error"})
    } else {
      res.status(error.status).json({message: error.message})
    }
  }
}

const getFiles = async (req, res) => {
  try {
    const list = await operations.getFiles(filesPath)
    res.status(200).json({message: "Success", files: list})
  } catch (error) {
if (error.status === undefined || error.status === 500) {
      res.status(500).json({message: "Server error"})
    } else {
      res.status(400).json({message: "Client error"})
    }
  }
}

const getFile = async (req, res) => {
  const { password } = req.query;
  const { filename } = req.params;
  try {
    await ps.verifyPassword({ filename, password })
      const { content, extension, uploadedDate } = await operations.getFile({ filename, filesPath })
      res.status(200).json({ message: "Success", filename, content, extension, uploadedDate })
    
  } catch (error) {
    if (error.status === undefined || error.status === 500) {
      res.status(500).json({message: "Server error"})
    } else {
      res.status(error.status).json({message: error.message})
    }
  }
}

const deleteFile = async (req, res) => {
  const { password } = req.query;
  const { filename } = req.params;
  try {
    await ps.verifyAndDeletePassvord({ filename, password })
    await operations.deleteFile({ filename, filesPath })
    res.status(200).json({ message: "File deleted successfully" })
  } catch (error) {
    if (error.status === undefined || error.status === 500) {
        res.status(500).json({message: "Server error"})
      } else {
        res.status(error.status).json({message: error.message})
      }
    
  }
}

const editFile = async (req, res) => {
  const { password } = req.query;
  const { filename } = req.params;
  const { content } = req.body;
  try {
    await ps.verifyPassword({filename, password})
    if (!content) {
      throw new CustomError(400,"Please specify 'content' parameter")
    }
    await operations.editFile({filename, filesPath, content})
    res.status(200).json({message: "File edited successfully"})
  } catch (error) {
    if (error.status === undefined || error.status === 500) {
      res.status(500).json({message: "Server error"})
    } else {
      res.status(error.status).json({message: error.message})
    }
    
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}
