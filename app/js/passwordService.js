const fs = require('fs/promises')
const path = require('path')
const CusromError = require('./CustomError')
const credsPath = path.join(__dirname, '../assets/creds.json')

const createPassword = async ({ filename, password }) => {
  try {
    const allPasswords = await getAllPasswords()
    allPasswords.push({ filename, password })
    await fs.writeFile(credsPath, JSON.stringify(allPasswords, null, 2))
  } catch (error) {
    throw error
  }
}

const getAllPasswords = async () => {
  try {
    const passwordsRow = await fs.readFile(credsPath, "utf8")
  return JSON.parse(passwordsRow)
  } catch (error) {
    throw error
  }
} 

const verifyPassword = async ({ filename, password }) => {
  try {
    const allPasswords = await getAllPasswords()
  const fileCreds = allPasswords.find(el => el.filename === filename)
  if (!fileCreds) return
    if (fileCreds.password !== password) {
    throw new CusromError(401, "Access denied")
  }
  } catch (error) {
    throw error
  }
}

const deletePassword = async ({ filename }) => {
  try {
    const allPasswords = await getAllPasswords()
  const filteredCreds = allPasswords.filter(el => el.filename !== filename)
  await fs.writeFile(credsPath, JSON.stringify(filteredCreds, null, 2))
  } catch (error) {
    throw error
  }
}

const verifyAndDeletePassvord = async ({ filename, password }) => {
  try {
    await verifyPassword({ filename, password })
  await  deletePassword({ filename })
  } catch (error) {
    throw error
  }
}

module.exports = {createPassword, verifyPassword, verifyAndDeletePassvord}