const path = require('path')
const CustomError = require('./CustomError')

const allowedExt = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

const validateFileExtention = (filename) => {
  const ext = path.extname(filename)
  const isValid = allowedExt.includes(ext)
  if (!isValid) {
    throw new CustomError(400,`Forbidden file extention ${ext}`)
  }
}

const validateFileName = (filename) => {
  const regex = /([a-zA-Z0-9\s_\\.\-\(\):])+\.([a-zA-Z])+$/i
  const isValid = regex.test(filename)
  if (!isValid) {
    throw new CustomError(400,`Forbidden file name`)
  }
}

const validateFile = (filename) => {
  validateFileName(filename)
  validateFileExtention(filename)
}

module.exports = validateFile
