const express = require('express')
const logger = require('morgan')
const cors = require('cors')

const controllers = require('./app/js/controllers')

const app = express()
const PORT = 8080;

app.use(cors())
app.use(logger("combined"))
app.use(express.json())


app.post('/api/files',  controllers.createFile)

app.get('/api/files', controllers.getFiles)

app.get('/api/files/:filename', controllers.getFile)

app.delete('/api/files/:filename', controllers.deleteFile)

app.put('/api/files/:filename', controllers.editFile)

app.use((req, res) => {
  res.status(404).json({ message: 'Not found' })
})

app.use((err, req, res, next) => {
  res.status(500).json({ message: "Server error" })
})

app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`)
})
